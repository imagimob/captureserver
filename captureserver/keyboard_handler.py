"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
​"""

import time
import keyboard


class SimpleKeyInputHandler():
    """
    Helper class for checking keyboard input. The keys that you want to check is defined by
    input_keys in __init__.
    """

    def __init__(self, input_keys, input_delay=1.0):
        self._last_command_registered_time = time.time()
        self._input_delay = input_delay
        self._input_keys = input_keys

    def check_keypress(self, keys=None):
        # Add input delay to avoid many commands being sent from just one keypress
        keys_to_check = keys if keys is not None else self._input_keys
        key_pressed = None
        current_time = time.time()
        elapsed_time = current_time - self._last_command_registered_time
        if elapsed_time > self._input_delay:
            for key in keys_to_check:
                if keyboard.is_pressed(key):
                    key_pressed = key
                    self._last_command_registered_time = current_time
                    break
        return key_pressed
