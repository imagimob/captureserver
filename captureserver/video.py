"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
​"""

import os
import time
import shutil
from multiprocessing import Process, Queue
from pathlib import Path

import cv2


# Recommended recording resolutions for OpenCvCameraRecorder
RESOLUTIONS = {
    "720p": (1280, 720),
    "540p": (960, 540),
    "360p": (640, 360)
}


class VideoRecorder():
    """
    Abstract base class defining an interface for a video recorder.
    """

    def __init__(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError

    def start_recording(self, output_file):
        raise NotImplementedError

    def stop_recording(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError


class OpenCvCameraRecorderError(Exception):
    pass


class OpenCvCameraRecorder(VideoRecorder):
    """
    OpenCV based video recorder that runs in a separate process.
    """

    # Commands for communicating with the video recording process
    _CLOSE = 0
    _START_RECORDING = 1
    _STOP_RECORDING = 2

    def __init__(self,
                 visualize=True,
                 fps: float = 30.0,
                 video_size: tuple = RESOLUTIONS["540p"],
                 fourcc: str = 'mp4v',
                 opencv_camera_index: int = 0):
        self.device_index = opencv_camera_index
        self.fps = fps
        self.fourcc = fourcc
        self.video_size = video_size

        self._command_queue = Queue()

        self.cv_fourcc = cv2.VideoWriter_fourcc(*self.fourcc)

        self.visualize = visualize

        # Process that runs the video recorder
        self.recorder_process = Process(
            target=self._recorder_loop_error_caught, args=[])

        # Process variables
        self.running = False
        self.recording = False
        self.video_writer = None
        self.recording_start_time = None
        self.recording_stop_time = None
        self.recording_frame_count = 0

        # Keep track of FPS fixing sub processes
        self._fps_fixers = []

    def run(self):
        """
        Run video recorder process.
        """
        self.running = True
        self.recorder_process.start()

    def start_recording(self, output_file: Path):
        """
        Initiate recording to output_file path
        """
        if not self.running:
            raise OpenCvCameraRecorderError("Cannot start recording without running the video recorder")
        self._command_queue.put((self._START_RECORDING, output_file))

    def stop_recording(self):
        """
        Stop active recording.
        """
        if not self.running:
            raise OpenCvCameraRecorderError("Cannot stop recording without running the video recorder")
        self._command_queue.put((self._STOP_RECORDING,))

    def close(self):
        """
        Close active recording process.
        """
        if not self.running:
            raise OpenCvCameraRecorderError("Cannot close video recorder without running it")
        self._command_queue.put((self._CLOSE,))

        # Wait for the recorder loop process to finish
        while self.recorder_process.is_alive():
            print("Waiting for recorder process to finish...")
            time.sleep(0.5)

    def _recorder_loop_error_caught(self):
        try:
            self._recorder_loop()
        except Exception as e:
            print(f"Unexpected error in OpenCvCameraRecorder: {e}")

    def _recorder_loop(self):
        """
        Internal loop that streams video from web camera and records to file.
        """
        video_cap = cv2.VideoCapture(self.device_index)

        # Run visualizer in a separate process
        if self.visualize:
            visualizer = OpenCvVideoVisualizer(window_size=self.video_size)
            visualizer.run()

        self.running = True
        self.recording = False
        self.video_writer = None

        while self.running:
            frame_time_start = time.time()
            ret, video_frame = video_cap.read()

            if ret:
                video_frame_resized = cv2.resize(video_frame, self.video_size)

                if self.visualize:
                    visualizer.update(video_frame_resized)

                if self.recording:
                    self.recording_frame_count += 1
                    self.video_writer.write(video_frame_resized)
                    if self.recording_start_time is None:
                        self.recording_start_time = frame_time_start

                self._handle_commands()

                frame_time_end = time.time()
                elapsed_frame_time = frame_time_end - frame_time_start
                sleep_time = (1.0 / self.fps) - elapsed_frame_time
                if sleep_time > 0.0:
                    time.sleep(sleep_time)
                else:
                    print("Warning: Video recording FPS might be set to too high")
            else:
                break

        video_cap.release()

        if self.visualize:
            visualizer.close()

        if self.video_writer is not None:
            self.video_writer.release()

        # Before closing we must make sure that all FPS fixers have finished processing.
        self._wait_for_fps_fixers()

    def _handle_commands(self):
        if self._command_queue.qsize() > 0:
            command_tuple = self._command_queue.get()
            command_type = command_tuple[0]

            if command_type == self._CLOSE:
                self.running = False
            elif command_type == self._START_RECORDING:
                self.recording = True
                output_path = command_tuple[1]
                self._init_video_writer(output_path)
            elif command_type == self._STOP_RECORDING:
                self._stop_recording()

    def _init_video_writer(self, output_path):
        self.current_recording_path = output_path
        self.video_writer = cv2.VideoWriter(str(output_path), self.cv_fourcc, self.fps, self.video_size)

    def _release_video_writer(self):
        if self.video_writer is not None:
            self.video_writer.release()
            self.video_writer = None

    def _stop_recording(self):
        self.recording_stop_time = time.time()
        self._release_video_writer()
        recording_total_time = self.recording_stop_time - self.recording_start_time
        measured_fps = float(self.recording_frame_count) / recording_total_time

        print(f"Wanted FPS: {self.fps}. Measured FPS: {measured_fps}")

        # Fix FPS of video since it is usually slightly off
        fps_fixer = OpenCvFpsFixer(
            video_file=self.current_recording_path,
            new_fps=measured_fps)
        fps_fixer.run()
        self._fps_fixers.append(fps_fixer)
        self._reset_recording_parameters()

    def _reset_recording_parameters(self):
        self.recording_start_time = None
        self.recording_stop_time = None
        self.recording_frame_count = 0
        self.recording = False

    def _wait_for_fps_fixers(self):
        """
        Wait for FPS fixer processes to finish, then return
        """
        for fps_fixer in self._fps_fixers:
            fps_fixer_process = fps_fixer.get_process()
            while fps_fixer_process.is_alive():
                print(f"Waiting for FPS fixer process to finish for file: {str(fps_fixer.video_file)}")
                time.sleep(0.5)


class OpenCvVideoVisualizer():
    """
    OpenCV based image visualizer that runs in separate process. Visualization is
    updated by pushing images to a queue that is popped by the visualization process.
    """

    # Commands for communicating with visualization process
    _CLOSE = 0

    def __init__(self, window_size=RESOLUTIONS["540p"]):
        self.window_name = "Video stream"
        self.window_size = window_size
        self._frame_queue = Queue()
        self._command_queue = Queue()
        self._visualization_process = None

    def update(self, frame):
        """
        Update visualization with new frame
        """
        self._frame_queue.put(frame)

    def close(self):
        """
        Stop the active visualization process
        """
        self._command_queue.put(OpenCvVideoVisualizer._CLOSE)

    def run(self):
        """
        Run visualization in separate process.
        """
        self._visualization_process = Process(
            target=self._run_visualization,
            args=[])
        self._visualization_process.start()
        return self._visualization_process

    def get_process(self):
        """
        Returns the multiprocessing.Process object running the visualization
        """
        return self._visualization_process

    def _run_visualization_error_caught(self):
        try:
            self._run_visualization()
        except Exception as e:
            print(f"Unexpected error in OpenCvVideoVisualizer: {e}")

    def _run_visualization(self):
        """
        Visualization loop to be called through multithreading process
        """
        cv2.namedWindow(self.window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(self.window_name, self.window_size)
        running = True
        while running:
            if self._frame_queue.qsize() > 0:
                frame = self._frame_queue.get()
                flipped_frame = cv2.flip(frame, 1)
                cv2.imshow(self.window_name, flipped_frame)
                cv2.waitKey(1)
            if self._command_queue.qsize() > 0:
                command = self._command_queue.get()
                if command == OpenCvVideoVisualizer._CLOSE:
                    running = False
            time.sleep(0.001)
        cv2.destroyAllWindows()


class OpenCvFpsFixer():
    """
    Class for updating FPS in a video file in a separate process.
    """

    def __init__(self, video_file: Path, new_fps: float):
        self.video_file = video_file
        self.new_fps = new_fps

    def run(self):
        """
        Run fps fixer process
        """
        self._fps_process = Process(
            target=self._run_process_error_caught,
            args=[])
        self._fps_process.start()

    def get_process(self):
        """
        Returns the multiprocessing.Process object running the fps fixing
        """
        return self._fps_process

    def _run_process_error_caught(self):
        try:
            self._run_process()
        except Exception as e:
            print(f"Unexpected error in OpenCvFpsFixer: {e}")

    def _run_process(self):
        """
        Visualization loop to be called through multithreading process
        """
        vcap = cv2.VideoCapture(str(self.video_file))
        width = int(vcap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        video_size = (width, height)
        fourcc = int(vcap.get(cv2.CAP_PROP_FOURCC))

        temp_out_path = self.video_file.parent / ("__temp_video__" + self.video_file.suffix)
        video_writer = cv2.VideoWriter(str(temp_out_path), fourcc, self.new_fps, video_size)

        while vcap.isOpened():
            ret, frame = vcap.read()
            if ret:
                video_writer.write(frame)
            else:
                break

        vcap.release()
        video_writer.release()

        # Swap files
        time.sleep(0.1)
        os.remove(str(self.video_file))
        time.sleep(0.1)
        shutil.move(str(temp_out_path), str(self.video_file))
        print(f"Updated FPS of video file: {self.video_file} to {self.new_fps}.")


class DummyVideoRecorder(VideoRecorder):
    """
    Empty implementation of VideoRecorder interface
    """

    def __init__(self):
        pass

    def run(self):
        pass

    def start_recording(self, output_file):
        pass

    def stop_recording(self):
        pass

    def close(self):
        pass
