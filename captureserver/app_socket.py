"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
​"""

import json
import socket
import struct

from typing import List


class AppDataSocket():
    """
    Class that defines the data streaming socket connection between the server and the capture app.
    """

    def __init__(self, ip: str = '0.0.0.0', port: int = 4711):
        self.ip = ip
        self.port = port

    def connect(self):
        print('Waiting for data socket connection...')

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((self.ip, self.port))
        sock.listen(1)

        self.connection, self.address = sock.accept()
        print('Connected to:', self.address)

    def send_data(self, data: bytes, time_seconds: float):
        """
        Send a sensor data packet over the socket with the timing information.
        """
        packet_type = 2
        int_size = 4
        packet_size = int_size * 2 + len(data)
        ti_sec = int(time_seconds)
        ti_micros = int((time_seconds - ti_sec) * 1000) * 1000

        data_packet_message = (
            socket.htonl(packet_type), socket.htonl(packet_size),
            socket.htonl(ti_sec), socket.htonl(ti_micros))
        packer = struct.Struct('4I')  # correct with unsigned int here?
        data_packet = packer.pack(*data_packet_message) + data

        try:
            self.connection.send(data_packet)
            return True
        except(BrokenPipeError, IOError) as e:
            print(e)
            print('Closing connection')
            self.disconnect(2)
            return False

    def disconnect(self, shutdown_signal: int):
        # shutdown_signal: 0 = done receiving; 1 = done sending; 2 = both
        try:
            # OSError seems to always be thrown when attempting to shutdown after
            # app has disconnected.
            self.connection.shutdown(shutdown_signal)
        except OSError:
            pass  # silently ignore this error
        self.connection.close()

    def send_info_packet(self, json_object: List[dict]):
        """
        Send the synch message to the app that describes the data format.
        """
        json_string = json.dumps(json_object)
        info_packet_message = \
            (socket.htonl(4), socket.htonl(len(json_string)), json_string.encode())  # size of buffer and type
        packer = struct.Struct('2I ' + str(len(json_string)) + 's')
        info_packet = packer.pack(*info_packet_message)
        self.connection.send(info_packet)
        print('Server sent info packet message:', info_packet_message)

    def recv_sync_packet(self):
        """
        Get ack response for the synch message from the app.
        """
        unpacker = struct.Struct('i')
        synch_packet_raw = self.connection.recv(unpacker.size)
        synch_packet = unpacker.unpack(synch_packet_raw)
        synch_packet = socket.ntohl(int(synch_packet[0]))

        if synch_packet == 1:
            print('Server received synch packet:', synch_packet)
        else:
            # TODO: Should we actually throw an error here?
            print('Error: did not receive synch packet')

    def get_address(self):
        return self.address


class AppCommandSocket():
    """
    Class that defines a socket connection with the app used for sending commands.
    """

    def __init__(self):
        # Port is set by the Capture app to 13337
        self.port = 13337
        self.sock = None

    def connect(self, ip: str):
        print('Waiting for command socket connection...')
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((ip, self.port))
        print(f'Command socket connected to: {ip}: {self.port}')

    def disconnect(self):
        self.sock.close()

    def send_start_record(self):
        self.sock.sendall(b"start-record\n")
        data = self.sock.recv(1024)
        print(f"Start record response: {data}")

    def send_stop_record(self):
        self.sock.sendall(b"stop-record\n")
        data = self.sock.recv(1024)
        print(f"Stop record response: {data}")
        directory_name = self._get_directory_name_from_response_data(data)
        return directory_name

    @staticmethod
    def _get_directory_name_from_response_data(data):
        return data.decode("utf-8").replace("stopping ", "").replace("\n", "")
