"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.

Imagimob Capture Server library.

Below code snippets shows an example on how to integrate a sensor with the captureserver lib.
To see more examples, download the captureserver repo and navigate to the 'examples' directory.

Code snippet:

###################################################

import numpy as np
from typing import List
from captureserver import CaptureServer
from captureserver import SensorConnection

class MySensorConnection(SensorConnection):
    # TODO: Implement all functions in this class

    def __init__(self, *args, **kwargs):
        # Initialize sensor
        pass

    def connect(self) -> None:
        # Connect to sensor
        pass

    def read_data(self) -> np.ndarray:
        # Read a data packet from sensor
        return np.array([1.0, 2.0, 3.0])

    def disconnect(self) -> None:
        # Disconnect from sensor
        pass

    def get_json_packet(self) -> List[dict]:
        # Describe the data format for the Capture App
        return [{"type": "float", "count": 3, "tag": "dummy"}]

# Run the server
sensor_connection = MySensorConnection()
server = CaptureServer(sensor_connection)
server.run()

###################################################

"""

from .capture_server import CaptureServer  # noqa
from .capture_server import CaptureServerError  # noqa

from .sensor_connection import SensorConnection  # noqa
from .sensor_connection import RandomNumberSensorConnection  # noqa

from .app_socket import AppDataSocket  # noqa
from .app_socket import AppCommandSocket  # noqa

from .capture_local import LocalDataCapture  # noqa
from .video import OpenCvCameraRecorder  # noqa
