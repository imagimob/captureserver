"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
​"""

import json
import sys
import time
from pathlib import Path

from .keyboard_handler import SimpleKeyInputHandler

from .app_socket import AppDataSocket
from .app_socket import AppCommandSocket
from .sensor_connection import SensorConnection


class CaptureServerError(Exception):
    pass


class CaptureServer(object):
    """
    Server that acts as a data pipeline from an arbitrary SensorConnection implementation
    to the Capture app.
    """

    SUPPORTED_DATA_TYPES = ['int', 'float']
    SUPPORTED_INPUT = {
        'q': {
            "description": "Close the server."
        },
        'r': {
            "description": "Send start recording command."
        },
        's': {
            "description": "Send stop recording command."
        }
    }

    def __init__(self,
                 sensor_connection: SensorConnection,
                 data_socket: AppDataSocket = AppDataSocket()):
        self.data_socket = data_socket
        self.sensor_connection = sensor_connection

        self.sensor_data_type = sensor_connection.get_json_packet()[0]['type']

        if self.sensor_data_type not in self.SUPPORTED_DATA_TYPES:
            raise CaptureServerError(
                f'Sensor data type "{self.sensor_data_type}" not supported. '
                f'Suppored types are: {self.SUPPORTED_DATA_TYPES}')

        if sys.byteorder == 'big':
            self.little_byteorder = False
        elif sys.byteorder == 'little':
            self.little_byteorder = True
        else:
            # Should not happen...
            raise CaptureServerError(f"Unkown system byteorder: {sys.byteorder}")

        # Internal variables configured when server is started
        self._command_socket = None
        self._key_input_handler = None
        self._server_running = False

    def run(self, enable_input=False):
        self.data_socket.connect()

        if enable_input:
            self._enable_keyboard_input()

        # Handshake between sensor and app
        json_object = self.sensor_connection.get_json_packet()
        self.data_socket.send_info_packet(json_object)
        self.data_socket.recv_sync_packet()
        self.sensor_connection.connect()

        packages_sent = 0
        self._server_running = True

        # Data stream loop will continue until app is disconnected
        while self._server_running:
            # Read data from sensor and create a timestamp for it
            sensor_data = self.sensor_connection.read_data()
            time_seconds = time.time()

            if sensor_data is None:
                continue

            # Convert data to correct format and send over socket
            bytes_data = self.prepare_data_for_socket(sensor_data)
            socket_status = self.data_socket.send_data(bytes_data, time_seconds)

            # If the app disconnects then send_data will return false
            if socket_status:
                packages_sent += 1
            else:
                self.close_server()

            # Handle keyboard input if enabled
            if enable_input:
                self._handle_keypress()

        self.data_socket.disconnect(2)
        self.sensor_connection.disconnect()

        print(f"Sent {packages_sent} packages.")

    def run_interactive_with_local_data(self, local_data_dir: Path):
        """
        Run with the server as the main controller of recording and store data to a local directory:
        - When recording is started from the server then it will start to dump the sensor data to local_data_dir
        - The sensor should be configured to return a buffer of data and the server will only send a single data
          point used for synchronization with the video
        """
        self.data_socket.connect()
        self._enable_keyboard_input()

        # Handshake between sensor and app
        # Set the count in the json object to 1 since we will only send one point as synch data
        # and store the rest in local_data_dir
        json_object = self.sensor_connection.get_json_packet()
        json_object[0]["count"] = 1
        self.data_socket.send_info_packet(json_object)
        self.data_socket.recv_sync_packet()
        self.sensor_connection.connect()

        # Create the data directory
        local_data_dir = Path(local_data_dir)
        local_data_dir.mkdir(parents=True, exist_ok=True)

        self._server_running = True

        # Data stream loop will continue until app is disconnected
        while self._server_running:
            # Check if user wants to start a recording
            keyboard_input = self._key_input_handler.check_keypress()

            if keyboard_input == 'r':
                self._record_with_local_data(local_data_dir)
            elif keyboard_input == 'q':
                self.close_server()

            # Read data from sensor and create a timestamp for it
            sensor_data = self.sensor_connection.read_data()
            time_seconds = time.time()

            if sensor_data is None:
                continue

            # Send a part of the sensor data to the app
            synch_data = sensor_data[0]
            bytes_data = self.prepare_data_for_socket(synch_data)
            socket_status = self.data_socket.send_data(bytes_data, time_seconds)

            # If the app disconnects then send_data will return false
            if not socket_status:
                self.close_server()

        self.data_socket.disconnect(2)
        self.sensor_connection.disconnect()

    def _record_with_local_data(self, root_data_dir):
        print("Recording started. Press 's' to stop the recording.")

        # Create a temporary directory for data storage
        recording_dir = Path(root_data_dir) / time.strftime("%Y-%m-%d_%H.%M.%S")
        recording_dir.mkdir()

        # Open a file to stream data to
        data_filepath = recording_dir / 'data.bin'
        data_file = open(data_filepath, "wb")
        byte_index = 0
        data_index = 0

        # Get information about the numpy data type and create a metadata container
        throwaway_data = self.sensor_connection.read_data()
        while throwaway_data is None:
            throwaway_data = self.sensor_connection.read_data()
        metadata = {
            "sensor": self.sensor_connection.get_json_packet(),
            "dtype": str(throwaway_data.dtype),
            "app_directory_name": None,
            "timestamps": []
        }

        # App directory name is returned when recording finishes
        app_directory_name = None

        self.send_start_recording_command()
        self.sensor_connection.read_data()  # read out the current data buffer
        recording_start_time = time.time()

        while True:
            # Check if user wants to stop recording
            if self._key_input_handler.check_keypress(keys=['s']) == 's':
                # Send stop recording command and get the directory name from the app
                app_directory_name = self.send_stop_recording_command()
                time.sleep(3.0)  # make sure the app has time to close the recording
                break

            sensor_data = self.sensor_connection.read_data()
            recording_time_seconds = time.time() - recording_start_time

            if sensor_data is None:
                continue

            # Write sensor data as bytes
            bytes_data = sensor_data.tobytes()
            data_file.write(bytes_data)
            byte_length = len(bytes_data)
            data_length = len(sensor_data)

            # Note the recording time for the current data packet
            metadata["timestamps"].append({
                "timestamp": recording_time_seconds,
                "byte_index": byte_index,
                "byte_length": byte_length,
                "data_index": data_index,
                "data_length": data_length
            })

            byte_index += byte_length
            data_index += data_length

            # Send synch data to app
            synch_data = sensor_data[0]
            bytes_data = self.prepare_data_for_socket(synch_data)
            socket_status = self.data_socket.send_data(bytes_data, recording_time_seconds)

            # If the socket has been closed by the app we will stop the recording
            if not socket_status:
                break

        # Close the binary file
        data_file.close()

        # Add the app directory name to metadata to be able to map to files on the phone
        if app_directory_name is not None:
            metadata["app_directory_name"] = app_directory_name

        # Store the timestamp information as a json
        metadata_filepath = recording_dir / 'metadata.json'
        with open(str(metadata_filepath), "w") as write_file:
            json.dump(metadata, write_file, indent=4)

        print(f"Recording stopped. Data stored here: {str(recording_dir)}")

    def prepare_data_for_socket(self, sensor_data):
        if self.sensor_data_type == 'int':
            sensor_data = sensor_data.astype('int32')
        else:
            # Assume float
            sensor_data = sensor_data.astype('float32')

        if self.little_byteorder:
            sensor_data = sensor_data.byteswap()

        return sensor_data.tobytes()

    def close_server(self):
        self._server_running = False

    def send_start_recording_command(self):
        self._command_socket.send_start_record()

    def send_stop_recording_command(self):
        return self._command_socket.send_stop_record()

    def _enable_keyboard_input(self):
        self._command_socket = AppCommandSocket()
        self._command_socket.connect(self.data_socket.get_address()[0])
        self._key_input_handler = SimpleKeyInputHandler(self.SUPPORTED_INPUT.keys())
        self._print_keyboard_input_description()

    def _print_keyboard_input_description(self):
        print("#" * 50)
        print("Supported keyboard input:")
        for key, command in self.SUPPORTED_INPUT.items():
            description = command["description"]
            print(f"{key} - {description}")
        print("#" * 50)

    def _handle_keypress(self):
        key = self._key_input_handler.check_keypress()
        if key:
            if key == 'q':
                self.close_server()
            elif key == 'r':
                self.send_start_recording_command()
            elif key == 's':
                self.send_stop_recording_command()
            else:
                print(f"Unsupported keyboard input registered: {key}")
