"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
​"""

import time
from pathlib import Path

from .keyboard_handler import SimpleKeyInputHandler
from .sensor_connection import SensorConnection
from .video import VideoRecorder
from .video import OpenCvCameraRecorder


class LocalDataCapture():
    """
    Capture data to local directory with web camera stream.
    """

    def __init__(self,
                 output_dir: Path,
                 sensor_connection: SensorConnection,
                 video_recorder: VideoRecorder = OpenCvCameraRecorder(),
                 data_filename="data.data",
                 label_filename="label.label",
                 video_filename="video.mp4"):
        self.output_root_dir = output_dir
        self.sensor_connection = sensor_connection
        self.data_filename = data_filename
        self.label_filename = label_filename
        self.video_filename = video_filename
        self._key_input_handler = SimpleKeyInputHandler(["r", "s", "q"])
        self._running = False
        self.video_recorder = video_recorder

    def run(self):
        """
        Run the local data capture system.
        """

        # Make sure the parent directory exists
        self.output_root_dir.mkdir(parents=True, exist_ok=True)
        self._running = True

        print(f"Recording data to: {self.output_root_dir}")
        self._print_keyboard_commands()

        # Start the video recorder process (does not trigger recording to file)
        if not self.video_recorder is None:
            self.video_recorder.run()

        self.sensor_connection.connect()

        # Data stream loop will continue until user quits via keypress
        while self._running:
            # Check if user wants to start a recording
            keyboard_input = self._key_input_handler.check_keypress()

            if keyboard_input == 'r':
                self._run_recording()
            elif keyboard_input == 'q':
                self._running = False

            # Read out data from sensor to keep it from buffering
            _ = self.sensor_connection.read_data()

        if not self.video_recorder is None:
            self.video_recorder.close()
        self.sensor_connection.disconnect()

    def _run_recording(self):
        print("Recording started. Press 's' to stop the recording.")

        # Create a temporary directory for data storage
        recording_dir = self.output_root_dir / time.strftime("%Y-%m-%d_%H-%M-%S")
        recording_dir.mkdir()

        # Open a file to stream data to
        data_filepath = recording_dir / self.data_filename
        data_file = open(data_filepath, "w")

        # Open a file to save labels to
        label_filepath = recording_dir / self.label_filename
        label_file = open(label_filepath, "w")
        label_file.write("Time(Seconds),Length(Seconds),Label(string),Confidence(double),Comment(string)")

        # Read some data from sensor to get header info
        throwaway_data = self.sensor_connection.read_data()
        while throwaway_data is None:
            throwaway_data = self.sensor_connection.read_data()

        throwaway_data = throwaway_data.flatten()
        header_str = "Time (seconds)," + ",".join([f"x{i}" for i in range(len(throwaway_data))])
        data_file.write(header_str + "\n")

        # Initiate video recording to file
        if not self.video_recorder is None:
            video_output_path = recording_dir / self.video_filename
            self.video_recorder.start_recording(video_output_path)

        # Read out sensor buffer
        _ = self.sensor_connection.read_data()
        recording_start_time = time.time()

        while True:
            # Check if user wants to stop recording
            if self._key_input_handler.check_keypress(keys=['s']) == 's':
                break

            sensor_data = self.sensor_connection.read_data()
            recording_time_seconds = time.time() - recording_start_time

            if sensor_data is None:
                continue

            # Write sensor data to file
            sensor_data_flat = sensor_data.flatten()
            data_file.write(f"{recording_time_seconds:.7f},")
            data_str = ",".join([f"{x:.7f}" for x in sensor_data_flat])
            data_file.write(data_str + "\n")

        if not self.video_recorder is None:
            self.video_recorder.stop_recording()

        # Close the output data file
        data_file.close()
        print(f"Recording stopped. Data stored here: {str(recording_dir)}")

    def _print_keyboard_commands(self):
        print("-" * 20 + " Keyboard input: " + "-" * 20)
        print("r - start recording")
        print("s - stop active recording")
        print("q - quit")
        print("-" * 50)
