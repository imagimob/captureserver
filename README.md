# Capture Server python library

The Capture Server library simplifies the integration process for new sensors with the Imagimob Capture system. 

**NOTE:** The preferred method for performing data collection is to follow the approach documented on the following page: https://developer.imagimob.com/data-preparation/data-collection/collect-data-using-graph-ux this process is more streamlined and is the best way to collect data. The Capture Server still exists to ensure that there is a solution for more complex setup.

**NOTE:** The Capture Server is only tested and supported on Windows and some Raspberry Pi models as mentioned below. It may require some modification for other operating systems

To integrate a new sensor and make it able to stream data to the Capture app; simply make an implementation of the abstract SensorConnection class. See code example below:

```python
import numpy as np
from typing import List
from captureserver import CaptureServer
from captureserver import SensorConnection

class MySensorConnection(SensorConnection):
    # TODO: Implement all functions in this class

    def __init__(self, *args, **kwargs):
        # Initialize sensor
        pass

    def connect(self) -> None:
        # Connect to sensor
        pass

    def read_data(self) -> np.ndarray:
        # Read a data packet from sensor
        return np.array([1.0, 2.0, 3.0])

    def disconnect(self) -> None:
        # Disconnect from sensor
        pass

    def get_json_packet(self) -> List[dict]:
        # Describe the data format for the Capture App
        return [{"type": "float", "count": 3, "tag": "dummy"}]

# Run the server
sensor_connection = MySensorConnection()
server = CaptureServer(sensor_connection)
server.run()
```
  
More examples can be found under the ```examples/``` directory.

## Capture App
The main purpose of the Capture Server is to forward data from a sensor to a connected Imagimob Capture app. See official documentation for how to setup the Capture app:  
http://developer.imagimob.com/imagimob-capture.html

## Local Data Capture
The ```LocalDataCapture``` class allows for capturing data without connecting to the Capture app, instead recording video using a web camera connected to the PC. See code example for how to run local data capture system:

```python
from pathlib import Path

from captureserver import LocalDataCapture
from captureserver import OpenCvCameraRecorder
from captureserver import RandomNumberSensorConnection
from captureserver.video import RESOLUTIONS

# Initiate an example sensor
sensor_connection = RandomNumberSensorConnection()

# Initiate video recorder using default web camera
video_recorder = OpenCvCameraRecorder(
    visualize=True,
    fps=30.0,
    video_size=RESOLUTIONS["540p"],
    opencv_camera_index=0)

# Start local capture system with a specified local directory to place data in
output_dir = Path("local/data/directory")
local_capture = LocalDataCapture(
    output_dir=output_dir,
    sensor_connection=sensor_connection,
    video_recorder=video_recorder)
local_capture.run()
```

A runnable example script is also provided: ```examples/dummy/record_local_data_example.py```.

# Installation
To install the captureserver library in your python environment, there are a couple of options to choose from:

* Clone the repo and run the setup.py script:
```
git clone https://git@bitbucket.org/imagimob/captureserver.git
cd captureserver
python setup.py install
```

* Install directly from git using pip:
```
pip install git+https://git@bitbucket.org/imagimob/captureserver.git         # install from master
pip install git+https://git@bitbucket.org/imagimob/captureserver.git@develop # install from develop branch
```

* *For developers:* Install in editable mode with pip (run from git root directory):
```
pip install -e .
```

# Examples
Below are instructions on how to run the different examples under the ```examples/``` directory. These examples require that the captureserver lib has been installed according to the above instructions.

## Simple Server Example
The ```capture_server_example.py``` is a very simple example to show how the integration between a SensorConnection and CaptureServer works. The sensor will return constant numbers of specified dimensionality and sampling time. Below shows an example call to the script:
```
cd examples/dummy
python capture_server_example.py --sensor-dimension 3 --sample-time 10
```
  
This will generate 3 constant numbers every 10 ms and send it to a connected Capture App. There is another flag called ```--enable-input``` that makes it possible to control recording from the server side using keyboard input.
  
More examples on how to integrate the CaptureServer with real sensors will be added in the near future.

## Acconeer Radar Capture Server
See [Acconeer Radar Capture Server documentation](examples/acconeer/README.md)

# Version Handling
The captureserver library is currently not deployed with complete library version control. If your application requires a specific version of the library to run, then the current recommended approach is to checkout the commit you want before installing the library.
  
Ideally this will be handled in the future by deploying the library e.g. on pypi: https://pypi.org/

# FAQ and Common Issues
## Installing Capture Server on a Raspberry Pi 3 Model B
Installing the Capture Server as explained above in a brand new Raspberry Pi 3 Model B may run into some issues related to the installation of the following dependencies: ```skbuild```, ```opencv```, ```numpy```, ```cython```, ```blas```, ```lapack```.

If this is the case, first of all, make sure that the Raspberry Pi OS is updated, that Python 3 is installed and that ```pip``` is updated. Also, consider using sudo together with ```pip3``` and ```python3``` to install packages and run Python scripts, respectively.

We recommend to start with a general upgrade of the installed packages using ```sudo apt full-upgrade``` and to upgrade pip as well with ```sudo pip3 install --upgrade pip``` or using ```sudo python3 -m pip install --upgrade pip```.

Then, in order to avoid installation issues, before running the Capture Server ```setup.py``` script, we recommend to install manually the following dependencies in the specified order:

1 - Install ```skbuild``` by running ```sudo pip3 install scikit-build``` and add to ```PATH``` the directory specified once the installation is completed, typically ```/home/pi/.local/bin``` for the default user ```pi```, with the command ```export PATH=/home/pi/.local/bin:$PATH``` to add the directory at the beginning of the ```PATH``` or using ```export PATH=$PATH:/home/pi/.local/bin``` to add the directory at the end of the ```PATH```.

2 - Install ```opencv``` using ```sudo pip3 install opencv-python``` or ```sudo apt-get install python3-opencv```.

3 - Install ```Cython``` with ```sudo pip3 install cython``` or upgrade it using ```sudo pip3  install cython --upgrade```.

4 - Install ```NumPy``` using ```sudo pip3 install numpy``` or upgrade it with ```sudo pip3  install numpy --upgrade```.

5 - Install ```blas``` and ```lapack``` dependencies by running the commands ```sudo apt-get install libblas-dev liblapack-dev``` and ```sudo apt install libatlas-base-dev```.