import setuptools

with open('README.md', encoding='utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name="captureserver",
    version="1.2.0",  # increment version when merging with master
    description="Library for setting up a Capture server",
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=[
        'bleak>=0.20.2',
        'keyboard>=0.13.4',
        'numpy>=1.18.2',
        'opencv-python>=4.4.0',
        'pandas>=2.0.3',
        'pyserial>=3.5',
        'wave>=0.0.2'
    ],
    packages=setuptools.find_packages(where='.'),
    python_requires='>=3.6',
    license="MIT",
)
