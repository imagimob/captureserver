"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.

This file shows an example of how to setup and use the LocalDataCapture class.
​"""

import argparse
from pathlib import Path

from captureserver.video import RESOLUTIONS
from captureserver.video import OpenCvCameraRecorder
from captureserver.capture_local import LocalDataCapture
from captureserver import RandomNumberSensorConnection


def main():
    args = parse_args()
    output_dir = Path(args.output_dir)

    # Create a dummy sensor connection
    sensor_connection = RandomNumberSensorConnection()

    # Setup recorder with default web camera
    resolution = RESOLUTIONS[args.video_resolution]
    fps = args.video_fps
    video_recorder = OpenCvCameraRecorder(
        visualize=True,
        fps=fps,
        video_size=resolution,
        opencv_camera_index=0)

    # Start local capture system
    local_capture = LocalDataCapture(
        output_dir=output_dir,
        sensor_connection=sensor_connection,
        video_recorder=video_recorder)
    local_capture.run()


def parse_args():
    parser = argparse.ArgumentParser(
        "Record random sensor data with synchronized webcam video stream.")
    parser.add_argument(
        '--output-dir', type=str, required=True,
        help='Directory to place data.')
    parser.add_argument(
        "--video-resolution",
        default="540p",
        type=str,
        choices=RESOLUTIONS.keys(),
        help="Select web camera video resolution. Default: 540p")
    parser.add_argument(
        "--video-fps",
        default=20.0,
        type=float,
        help="Specify desired FPS of video recording. Default: 20.0")
    return parser.parse_args()


if __name__ == '__main__':
    main()
