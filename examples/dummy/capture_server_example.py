"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.

This file shows an example of how to integrate a custom sensor connection,
the MySensorConnection class below, with the CaptureServer.
​"""

import argparse
import time
from typing import List

from captureserver import CaptureServer
from captureserver import SensorConnection

import numpy as np


class MySensorConnection(SensorConnection):

    def __init__(self, dimensions, sample_time):
        # Initialize sensor
        self._dimensions = dimensions
        self._sample_time = sample_time
        self._last_sensor_reading = time.time()

    def connect(self) -> None:
        # Since we are not connecting to anything we don't need to implement this function
        pass

    def read_data(self) -> np.ndarray:
        # Generate an array of self._dimension size and return it if self._sample_time time
        # has passed, otherwise return None.

        elapsed_time = time.time() - self._last_sensor_reading
        if elapsed_time > self._sample_time:
            sensor_data = np.array(range(self._dimensions))
            self._last_sensor_reading = time.time()
        else:
            sensor_data = None

        return sensor_data

    def disconnect(self) -> None:
        pass

    def get_json_packet(self) -> List[dict]:
        # Describe the data format for the Capture App
        return [{"type": "float", "count": self._dimensions, "tag": "dummy"}]


def parse_args():
    parser = argparse.ArgumentParser(
        "Example Capture server main function using a dummy sensor that returns constant values.")
    parser.add_argument(
        '--sensor-dimensions', type=int, default=10,
        help='Select the dimensionality of the sensor.')
    parser.add_argument(
        '--sample-time', type=int, default=10,
        help='Select the sample time of the sensor in ms, defauls to 10 (100 Hz).')
    parser.add_argument(
        '--enable-input', action='store_true',
        help='Enables keyboard input for the server.')
    return parser.parse_args()


def main():
    args = parse_args()

    sample_time_seconds = float(args.sample_time) / 1000.0

    # Initialize the sensor connection
    sensor_connection = MySensorConnection(
        dimensions=args.sensor_dimensions,
        sample_time=sample_time_seconds)

    # Create a CaptureServer instance with our sensor connection
    capture_server = CaptureServer(sensor_connection)

    # Start the server
    capture_server.run(enable_input=args.enable_input)


if __name__ == '__main__':
    main()
