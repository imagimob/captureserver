"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
"""

import argparse
from pathlib import Path

from captureserver import LocalDataCapture
from generic_connection_ble import *
from generic_connection_serial import *
from generic_connection_tcp import *
from captureserver.video import OpenCvCameraRecorder
from captureserver.video import RESOLUTIONS

import numpy as np
import pandas as pd
import sys
import wave


def parse_args():
    parser = argparse.ArgumentParser(
        "Capture server for generic device connected to BLE/Serial/TCP port. Expected data bytes array.")

    # COMMUNICATION INTERFACE ARGUMENTS
    parser.add_argument(
        "--protocol",
        type=str,
        help="Communication protocol to use for data transfers. [BLE, Serial, TCP] Default: Serial")

    parser.add_argument(
        "--device-name", "-d",
        type=str,
        help="Name of the device to connect to. (BLE or Serial only. BLE required, Serial if --COM-port not set)")

    parser.add_argument(
        "--uuid", "-u",
        type=str,
        help="Specify the UUID of the characteristic to read data from. (BLE only; required)")

    parser.add_argument(
        "--ip-address", "-ip",
        type=str,
        help="IP Address of the device to connect to. (TCP only; required)")

    parser.add_argument(
        "--port",
        type=int,
        help="Specify the port to use for transferring data. (TCP only; required)")

    parser.add_argument(
        "--COM-port", "-p",
        type=str,
        help="COM port where the device is connected. (Serial only; use this or --device-name)")

    parser.add_argument(
        "--baudrate", "-br",
        default=9600,
        type=int,
        help="Select connection baudrate. Default: 9600 (Serial only)")

    parser.add_argument(
        "--timeout",
        default=None,
        type=float,
        help="Select connection timeout. Default: None (Serial only)")

    parser.add_argument(
        '--start-msg',
        default=None,
        type=str,
        help='Message used to start data transfer. Default: None (Serial only)')

    parser.add_argument(
        '--stop-msg',
        default=None,
        type=str,
        help='Message used to stop data transfer. Default: None (Serial only)')

    # DATA PROCESSING ARGUMENTS
    parser.add_argument(
        "--data-type", "-dt",
        default='h',
        type=str,
        help="Select sample type supported by 'struct' Python library. Default: 'h' corresponding to 16 bits/2 bytes integers.")

    parser.add_argument(
        "--endianess", "-e",
        default='@',
        type=str,
        help="Select sample endianess supported by 'struct' Python library. Default: '@' corresponding native endianess.")

    parser.add_argument(
        '--features', "-fs",
        default=1,
        type=int,
        help='Select the number of features/channels or types of data.')

    parser.add_argument(
        "--samples-per-packet", "-sspp",
        default=60,
        type=int,
        help="Select the number of samples in each packet received. Must be a multiple of the number of features. Default: 60")

    parser.add_argument(
        "--sample-rate",
        default=50,
        type=int,
        help="The number of samples to attempt to collect per second. Default: 50")

    # OUTPUT FILE ARGUMENTS
    parser.add_argument(
        '--data-format',
        default='.data',
        type=str,
        choices=['.data', '.wav'],
        help='The type of file to generate. CSV .data file, or audio .wav file. Default: .data')

    parser.add_argument(
        '--data-filename',
        default='sensor',
        type=str,
        help='Name of the data file to be saved. Default: sensor')

    parser.add_argument(
        "--output-dir", "-o",
        type=str,
        required=True,
        help="Directory where to place the collected data.")

    # VIDEO CAPTURE ARGUMENTS
    parser.add_argument(
        "--video-disabled",
        action='store_true',
        help="Disable video capture.")

    parser.add_argument(
        "--video-resolution",
        default="540p",
        type=str,
        choices=RESOLUTIONS.keys(),
        help="Select web camera video resolution. Default: 540p.")

    parser.add_argument(
        "--video-fps",
        default=20.0,
        type=float,
        help="Select FPS of video recording. Default: 20.")

    parser.add_argument(
        "--video-filename",
        default="video.mp4",
        type=str,
        help="Select name of output video files. Default: video.mp4.")

    parser.add_argument(
        "--opencv-camera-index",
        default=0,
        type=int,
        help="Specify index OpenCV based index of camera stream. Default: 0.")

    return parser.parse_args()


def set_im_studio_format(data_np, data_reshaped, samples_per_packet, number_of_features):
    # parse dataset to IM Studio format
    repeat_number = samples_per_packet // number_of_features
    #print(repeat_number)
    data_np_timestamps = np.repeat(data_np[1:, 0], repeat_number)
    #print(data_np_timestamps[0:40])

    row_index = 0
    while row_index < data_np_timestamps.shape[0]:
        timestamp = data_np_timestamps[row_index]
        start_index = row_index
        end_index = row_index
        row_index += 1
        # Selecting rows with same timestamp - data samples belonging to same data packet
        if row_index < data_np_timestamps.shape[0]:
            for index in range(row_index, data_np_timestamps.shape[0]):
                timestamp_next = data_np_timestamps[index]
                if timestamp == timestamp_next:
                    end_index = index
                else:
                    break

            if end_index == start_index:
                start_time = 0
                #continue
            elif end_index > start_index:
                if start_index == 0:
                    start_time = 0
                else:
                    start_time = data_np_timestamps[start_index - 1]
                end_time = data_np_timestamps[end_index]
                delta_time = (end_time - start_time) / (end_index - start_index + 1)
                # Rescaling timestamps in a data packet so that they have unique values
                scale_array_np = np.arange(0, repeat_number, dtype=int)[::-1]
                data_np_timestamps[start_index:end_index+1] -= scale_array_np[:]*delta_time
                row_index = end_index + 1

    data_np_out_no_header = np.c_[data_np_timestamps, data_reshaped]
    #print(data_np_out_no_header[0:11])

    # Setting up header
    header = ['x{}'.format(feature) for feature in range(number_of_features)]
    header.insert(0, 'Time (seconds)')

    # creating output dataframe with header
    data_df_out = pd.DataFrame(data_np_out_no_header, columns=header)
    #print(data_df_out.head())

    # Removing rows with timestamp value larger than minimum timestamp occurring before minimum
    index_min_timestamp = data_df_out.idxmin()[0]

    if index_min_timestamp > 0:
        for row_index in range(index_min_timestamp):
            data_df_out.drop([row_index], inplace=True)

    data_df_out.reset_index(inplace=True, drop=True)

    return data_df_out


def save_array_to_wave(audio_np, num_channels, sample_rate, data_type, output_file_path):
    """
    Save np array to wave file
    """

    sample_bytes_size = get_sample_bytes_size(data_type)
    if data_type == 'h':
        audio_scaled_np = np.int16(audio_np)
    elif data_type == 'H':
        audio_scaled_np = np.uint16(audio_np)
    elif data_type == 'i' or  data_type == 'l':
        audio_scaled_np = np.int32(audio_np)
    elif data_type == 'I' or data_type == 'L':
        audio_scaled_np = np.uint32(audio_np)
    elif data_type == 'q':
        audio_scaled_np = np.int64(audio_np)
    elif data_type == 'Q':
        audio_scaled_np = np.uint64(audio_np)
    elif data_type == 'f' or data_type == 'd':
        audio_scaled_np = np.int16(audio_np * 32767)   # saving audio file as int16
        sample_bytes_size = 2
    else:
        audio_scaled_np = audio_np

    with wave.open(output_file_path, "w") as f:
        # 2 Channels.
        f.setnchannels(num_channels)
        # 2 bytes per sample.
        f.setsampwidth(sample_bytes_size)
        f.setframerate(sample_rate)
        f.writeframes(audio_scaled_np.tobytes())


def search_for_data_files(search_dir: Path, data_file_wildcard: str):
    """
    Searches recursively for files named according to data_file_wildcard
    """
    data_files = list(search_dir.rglob(data_file_wildcard))
    if not data_files:
        print(f"Warning: Could not find any data files named: {data_file_wildcard}")

    return data_files


def get_sample_bytes_size(data_type):
    c_type_dict = {
        'h': 2, # short
        'H': 2, # unsigned short
        'i': 4, # int
        'I': 4, # unsigned int
        'l': 4, # long
        'L': 4, # unsigned long
        'q': 8, # long long
        'Q': 8, # unsigned long long
        'f': 4, # float
        'd': 8  # double
    }
    try:
        sample_bytes_size = c_type_dict[data_type]
    except:
        print("---------------------------------------------------------------------")
        print(f"-----------------     Data type {data_type} not supported   -----------------")
        print("---------------------------------------------------------------------")
        return

    return sample_bytes_size


def setup_sensor_connection(args):
    # Setup sensor connection
    data_type = args.data_type  # default 'h'
    number_of_features = args.features
    sample_bytes_size = get_sample_bytes_size(data_type)
    samples_per_packet = args.samples_per_packet    # default 60
    sample_rate = args.sample_rate
    endianess = args.endianess  # default '@'

    if args.protocol == "BLE" or args.protocol == "ble":
        device_name = args.device_name
        device = auto_detect_ble_device(device_name)
        sensor_uuid = args.uuid

        return BytesBleConnection(device, sensor_uuid, number_of_features, samples_per_packet, sample_bytes_size, sample_rate, data_type, endianess)
    elif args.protocol == "Serial" or args.protocol == "serial" or args.protocol == "UART" or args.protocol == "uart":
        serial_port = args.COM_port
        device_name = args.device_name

        if serial_port is None:
            serial_port = auto_detect_com_port(device_name)

        # Set up sensor serial connection
        baudrate = args.baudrate
        timeout = args.timeout
        start_message = args.start_msg
        stop_message = args.stop_msg

        return BytesSerialConnection(serial_port, baudrate, timeout, start_message, stop_message, samples_per_packet, sample_bytes_size, data_type, endianess)
    elif args.protocol == "TCP" or args.protocol == "tcp":
        ip_address = args.ip_address
        port = args.port

        return BytesTcpConnection(ip_address, port, number_of_features, samples_per_packet, sample_bytes_size, sample_rate, data_type, endianess)
    return None

def main():
    args = parse_args()
    output_dir = Path(args.output_dir)

    # Set up the video recorder process
    video_disabled = args.video_disabled
    video_resolution = RESOLUTIONS[args.video_resolution]
    video_fps = args.video_fps
    camera_index = args.opencv_camera_index

    video_recorder = OpenCvCameraRecorder(
        visualize=True,
        fps=video_fps,
        video_size=video_resolution,
        opencv_camera_index=camera_index) if not video_disabled else None

    sensor_connection = setup_sensor_connection(args)

    # Run the local capture system
    data_format = args.data_format
    data_filename = args.data_filename + '.raw'
    video_filename = args.video_filename

    local_data_capture = LocalDataCapture(
        output_dir=output_dir,
        sensor_connection=sensor_connection,
        video_recorder=video_recorder,
        data_filename=data_filename,
        video_filename=video_filename)

    try:
        local_data_capture.run()
    except Exception as e:
        print("--------------------------------------------------------------------------------------------")
        print("---------------------     Device not found or another error occurred   ---------------------")
        print("--------------------------------------------------------------------------------------------")
        print(e)
        local_data_capture.video_recorder.close()
        sys.exit()


    # Post-processing data and creating output file to match Imagimob Studio format
    parsed_data_filename = args.data_filename + data_format
    number_of_features = args.features

    data_files = search_for_data_files(output_dir, data_filename)

    for data_file in data_files:
        print("------------------------")
        print("Data File:", data_file)

        if (Path(data_file.parent) / parsed_data_filename).is_file():
            print('No new data to parse.')
            print("------------------------")
        else:
            print("Data parsing...")
            data_np = np.genfromtxt(data_file, delimiter=',')
            data_np_no_header_no_time = data_np[1:, 1:]

            #time_in = time.perf_counter()

            # saving final result to csv (.data) or wav file
            output_file = data_file.parent.as_posix() + '/' + parsed_data_filename
            if data_format == '.data':
                # reshaping data according to expected number of features
                data_np_reshaped = data_np_no_header_no_time.reshape((data_np_no_header_no_time.shape[0]*data_np_no_header_no_time.shape[1] // number_of_features, number_of_features))
                # changing timestamps to match Imagimob Studio format
                data_df_im_studio = set_im_studio_format(data_np, data_np_reshaped, args.samples_per_packet, number_of_features)
                data_df_im_studio.to_csv(output_file, index=False)
            else:
                save_array_to_wave(data_np_no_header_no_time, args.features, args.sample_rate, args.data_type, output_file)

            #time_out = time.perf_counter()
            #print(f"TIMING: {time_out-time_in:0.4f}")

            print("Data File Parsed.")
            print("----------------------------")


if __name__ == "__main__":
    main()
