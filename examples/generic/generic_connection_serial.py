from captureserver import SensorConnection
from typing import List
import numpy as np
import serial
import time
import struct


def auto_detect_com_port(device_name):
    print("---------------------------------------------")
    print ('List of devices connected on COM ports : ')
    print("---------------------------------------------")
    COM_ports = list(serial.tools.list_ports.comports())
    for p in COM_ports:
        print(p)
    serial_port = ''
    for COM_port in COM_ports:
        if device_name in COM_port[1]:
            serial_port = COM_port[0]
            print("-------------------------------------------------------")
            print("{} connected on port << {} >>".format(device_name, serial_port))
            print("-------------------------------------------------------")
            break

    return serial_port

class BytesSerialConnection(SensorConnection):

    def __init__(self, port: str,
                baudrate: int,
                timeout: int,
                start_message,
                stop_message,
                samples_per_packet,
                sample_bytes_size,
                data_type: str,
                endianess: str):

        # Initialize UART sensor
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.start_message = start_message
        self.stop_message = stop_message
        self.samples_per_packet = samples_per_packet
        self.sample_bytes_size = sample_bytes_size
        self.data_type = data_type
        self.endianess = endianess


    def connect(self) -> None:
        # Connect to device over serial port.
        self.serial = serial.Serial(self.port,
                                    self.baudrate,
                                    bytesize=serial.EIGHTBITS,
                                    timeout=self.timeout,
                                    stopbits=serial.STOPBITS_ONE,
                                    xonxoff=False,
                                    rtscts=False,
                                    dsrdtr=False)
        self.serial.close()
        time.sleep(0.1)
        self.serial.open()

        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()

        if not self.serial.is_open:
            return False

        if self.start_message is not None:
            self.serial.write(str.encode(self.start_message))

        return True


    def read_data(self) -> np.ndarray:
        # resetting input and output buffers
        #self.serial.reset_input_buffer()
        #self.serial.reset_output_buffer()

        # reading data from serial port
        #time_in = time.perf_counter()

        data_packet = self.serial.read(self.samples_per_packet * self.sample_bytes_size)
        ####data_packet = self.serial.readline()

        #time_out = time.perf_counter()
        #print("DATA LEN", len(data_packet))
        #print(f"TIMING: {time_out-time_in:0.4f}") # @16 kHz expected timing: 1024/16000 = 0.064 secs

        # unpacking data packet
        unpack_format = '{}{}{}'.format(self.endianess, self.samples_per_packet, self.data_type)
        #print(f"Data: {data_packet}    Unpacked: {unpack_format}")
        data_unpacked_np = np.array(struct.unpack(unpack_format, data_packet))
        #print(len(data_unpacked_np))

        return data_unpacked_np


    def disconnect(self) -> None:
        # Disconnect from serial port.
        if self.stop_message is not None:
            self.serial.write(self.stop_message)

        self.serial.close()
        pass


    def get_json_packet(self) -> List[dict]:
        # Ignore this method.
        pass
