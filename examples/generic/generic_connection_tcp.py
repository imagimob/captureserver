from captureserver import SensorConnection
from typing import List
import numpy as np
import time
import socket
import struct

class BytesTcpConnection(SensorConnection):

    def __init__(self,
                ip_address: str,
                port: int,
                number_of_features: int,
                samples_per_packet: int,
                sample_bytes_size: int,
                sample_rate: int,
                data_type: str,
                endianess: str):

        # Initialize TCP socket interface
        self.ip_address = ip_address
        self.port = port
        self.number_of_features = number_of_features
        self.samples_per_packet = samples_per_packet
        self.sample_bytes_size = sample_bytes_size
        self.data_type = data_type
        self.endianess = endianess
        self.sample_rate = (1000 / sample_rate) * (samples_per_packet / number_of_features)
        self.time_stamp = 0


    def connect(self) -> None:
        # Connect to ip_address.

        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 10)
            self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 1)
            self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 2)
            self.socket.connect((self.ip_address, self.port))
        except Exception as e:
            print('Error: '+ str(e))
            return False

        return True


    def read_data(self) -> np.ndarray:
        delay = self.time_stamp - round(time.time()*1000) + self.sample_rate
        if (self.time_stamp > 0):
        #    print(f"sample_rate: {self.sample_rate}   delay: {delay}")
            if (delay > 1):
                time.sleep((delay-1)/1000)
        #print(f"sample_rate: {self.sample_rate}   delay: {delay}    delayed: {round(time.time()*1000) - self.time_stamp}")
        self.time_stamp = round(time.time()*1000)

        data_packet = self.socket.recv(self.samples_per_packet * self.sample_bytes_size)
        #print('read: ', data_packet)
        #print('read: ', bytearray.decode(data_packet))

        #time_out = time.perf_counter()
        #print("DATA LEN", len(data_packet))
        #print(f"TIMING: {time_out-time_in:0.4f}") # @16 kHz expected timing: 1024/16000 = 0.064 secs

        # unpacking data packet
        unpack_format = '{}{}{}'.format(self.endianess, self.samples_per_packet, self.data_type)
        #print(f"Data: {data_packet}    Unpacked: {unpack_format}")
        data_unpacked_np = np.array(struct.unpack(unpack_format, data_packet))
        #print(len(data_unpacked_np))

        return data_unpacked_np


    def disconnect(self) -> None:
        #self.sock.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        pass


    def get_json_packet(self) -> List[dict]:
        # Ignore this method.
        pass
