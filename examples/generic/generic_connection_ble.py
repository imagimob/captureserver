from captureserver import SensorConnection
from typing import List
from bleak import BleakScanner
from bleak import BleakClient
from bleak import BLEDevice
import asyncio
import numpy as np
import time
import struct


#notified = False
#collected_data = []

#def notify_callback(handle, data):
#    global collected_data
#    global notified
#
#    print(f"Notify: {data}")
#    collected_data = data
#    notified = True


def auto_detect_ble_device(device_name):
    print("---------------------------------------------")
    print('List of devices connected via BLE : ')
    print("---------------------------------------------")
    devices = asyncio.run(BleakScanner.discover())
    for d in devices:
        print(d)
    device = asyncio.run(BleakScanner.find_device_by_name(device_name))
    if device:
        print("-------------------------------------------------------")
        print("Connected to device << {} >>".format(device))
        print("-------------------------------------------------------")

    return device


class BytesBleConnection(SensorConnection):

    def __init__(self,
                device: BLEDevice,
                sensor_uuid: str,
                number_of_features: int,
                samples_per_packet: int,
                sample_bytes_size: int,
                sample_rate: int,
                data_type: str,
                endianess: str):

        # Initialize BLE device
        self.device = device
        self.sensor_uuid = sensor_uuid
        self.number_of_features = number_of_features
        self.samples_per_packet = samples_per_packet
        self.sample_bytes_size = sample_bytes_size
        self.data_type = data_type
        self.endianess = endianess
        self.sample_rate = (1000 / sample_rate) * (samples_per_packet / number_of_features)
        self.time_stamp = 0


    def connect(self) -> None:
        # Connect to device.

        self.client = BleakClient(self.device)
        asyncio.run(self.client.connect())
        asyncio.run(self.client.pair())

        if not self.client.is_connected:
            return False

        #asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
        #asyncio.run(self.client.start_notify(self.sensor_uuid, notify_callback))
        #asyncio.new_event_loop().create_task(self.client.start_notify(self.sensor_uuid, notify_callback))
        #asyncio.new_event_loop().run_until_complete(self.client.start_notify(self.sensor_uuid, notify_callback))

        return True


    def read_data(self) -> np.ndarray:
        #global collected_data
        #global notified

        delay = self.time_stamp - round(time.time()*1000) + self.sample_rate
        if (self.time_stamp > 0):
        #    print(f"sample_rate: {self.sample_rate}   delay: {delay}")
            if (delay > 1):
                time.sleep((delay-1)/1000)
        #print(f"sample_rate: {self.sample_rate}   delay: {delay}    delayed: {round(time.time()*1000) - self.time_stamp}")
        self.time_stamp = round(time.time()*1000)

        data_packet = asyncio.run(self.client.read_gatt_char(self.sensor_uuid))
#        data_packet = collected_data if notified else asyncio.run(self.client.read_gatt_char(self.sensor_uuid))
#        notified = False
#        asyncio.new_event_loop().run_until_complete(self.client.start_notify(self.sensor_uuid, notify_callback))


        #print('read: ', data_packet)
        #print('read: ', bytearray.decode(data_packet))

        #time_out = time.perf_counter()
        #print("DATA LEN", len(data_packet))
        #print(f"TIMING: {time_out-time_in:0.4f}") # @16 kHz expected timing: 1024/16000 = 0.064 secs

        # unpacking data packet
        unpack_format = '{}{}{}'.format(self.endianess, self.samples_per_packet, self.data_type)
        #print(f"Data: {data_packet}    Unpacked: {unpack_format}")
        data_unpacked_np = np.array(struct.unpack(unpack_format, data_packet))
        #print(len(data_unpacked_np))

        return data_unpacked_np


    def disconnect(self) -> None:
        # Disconnect from device.
        try:
            print("disconnecting")
            #asyncio.run(self.client.unpair())
            #asyncio.run(self.client.disconnect())
        except:
            print("Error disconnecting")

        pass


    def get_json_packet(self) -> List[dict]:
        # Ignore this method.
        pass
