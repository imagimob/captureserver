# General Purpose Capture Server Example

This example shows how to integrate the Capture Server with any board streaming sensor or audio data. It is designed to support a range of communication protocols including BLE, Serial (UART or USB CDC), or TCP. The data from the board is expected to be sent in packets (array of bytes) with a fixed number of samples/values of a certain type and a specific endianess. The command will start a server with the board connected via the specified protocol. The choice of the available parameters/arguments should be adapted to suit your application.

When capturing sensor data, the data will be stored into a CSV file tracking each feature. By default, it will also capture a video via a webcam to show exactly what was being done at the time the sensor captured the data. After quitting the data capture, the collected data is parsed using the value of the ```--features``` argument into a CSV format with timestamps suitable for Imagimob Studio.

When capturing audio data, the data will be stored into a WAV file that can be listened to later. To achieve that you need to disable the video recording using the ```--video-disabled``` argument and set the output data format to .wav using the ```--data-format .wav``` argument.


## Running the Capture Server

The example for local capture can be started, for instance, by running a command like the examples below. Data will be recorded locally on the PC together with the video from a web camera.

BLE:
```bash
python generic_local_capture_interface.py --output-dir <path to desired output folder> --protocol BLE --device-name <Device Name> --uuid <UUID of the characteristic to get data from> --data-type <type of data that the board is streaming to PC> --samples-per-packet <number of values/samples which are contained in each received packet> --features <number of features/columns used to reshape the packet during data parsing>
```

Serial:
```bash
python generic_local_capture_interface.py --output-dir <path to desired output folder> --protocol serial --COM-port COM<COM port number where board is connected to PC> --baudrate <baudrate at which the board is streaming data to PC> --data-type <type of data that the board is streaming to PC> --samples-per-packet <number of values/samples which are contained in each received packet> --features <number of features/columns used to reshape the packet during data parsing>
```

TCP:
```bash
python generic_local_capture_interface.py --output-dir <path to desired output folder> --protocol TCP --ip-address <IP Address> --port <port to connect to> --data-type <type of data that the board is streaming to PC> --samples-per-packet <number of values/samples which are contained in each received packet> --features <number of features/columns used to reshape the packet during data parsing>
```

## Data capture

The capture interface supports two general flavors of data being captured: sensor data and audio data. Sensor data is organized into csv files with a .data extension. Audio data is stored into a .wav file.

### Sensor capture

When capturing sensor data, a timestamp is automatically attached to the data. This is used to keep track of when events occur. The timestamp is set based on when the packet of data was received. In the case that multiple frames are combined into a single packet (typical) the tool will automatically redistribute the features when quitting out of the server. In this case the samples are evenly distributed between the last time stamp and the current one. This will give each frame a unique timestamp.

To capture a 3-axis floating point accelerometer sensor output at 50Hz over a serial connection on COM82 at 115200 baud; the following command can be used:
NOTE: This processes blocks of 240 bytes at a time (4 bytes per sample * 60 samples per packet)
```bash
python generic_local_capture_interface.py --output-dir data --protocol serial --COM-port COM82 --baudrate 115200 --data-type f --samples-per-packet 60 --features 3
```

### Audio capture

To capture 16-bit signed stereo PDM audio data sampled at 16kHz over a serial connection on COM82 at 921600 baud; the following command can be used:
NOTE: This processes blocks of 120 bytes at a time (2 bytes per sample * 60 samples per packet) and disables the video stream
```bash
python generic_local_capture_interface.py --output-dir data --protocol serial --COM-port COM82 --baudrate 921600 --data-format .wav --data-type h --samples-per-packet 60 --features 2 --sample-rate 16000 --video-disabled
```

## Command line arguments

The capture server offers a wide range of options for collecting data. The full set of options can be seen below or is available using the `--help` flag.

### Communication protocol options

As mentioned above, the server supports multiple communication protocols, each with its own requirements. The arguments necessary for each protocol are listed below.

   * `--protocol`: Communication protocol to use for data transfers. [BLE, Serial, TCP] Default: Serial"

Protocol specific options:

   * BLE options:
        * `--device-name/-d <value>`: Name of the device to connect to.
        * `--uuid/-u <value>`: Specify the UUID of the characteristic to read data from.

   * Serial options:
        * `--device-name/-d <value>`: Name of the device to connect to. (if `--COM-port` not set)
        * `--COM-port/-p <value>`: COM port where the device is connected. (if `--device-name` not set)
        * `--baudrate/-br <value>`: Select connection baudrate. Default: 9600
        * `--timeout <value>`: Select connection timeout. Default: None
        * `--start-msg <value>`: Message used to start data transfer. Default: None
        * `--stop-msg <value>`: Message used to stop data transfer. Default: None

   * TCP options:
        * `--ip-address/-ip <value>`: IP Address of the device to connect to.
        * `--port <value>`: Specify the port to use for transferring data.

### Data format options

The data from the board is expected to be sent in packets (array of bytes) with a fixed number of samples/values of a certain type and a specific endianess. The server supports a variety of formats which can specified with the following arguments.

   * `--data-type/-dt <value>`: Select sample type supported by 'struct' Python library. Default: `h`. The supported data types are:
       * `h`: short
       * `H`: unsigned short
       * `i`: int
       * `I`: unsigned int
       * `l`: long
       * `L`: unsigned long
       * `q`: long long
       * `Q`: unsigned long long
       * `f`: float
       * `d`: double
   * `--endianess/-e`: Select sample endianess supported by 'struct' Python library. Default: `@`. The supported endianess types are:
        * `@` : native
        * `=` : native
        * `<` : little-endian
        * `>` : big-endian
        * `!` : network (= big-endian)
    * `--features/-fs`: Select the number of features/channels or types of data. Default: 1
    * `--samples-per-packet/-sspp`: Select the number of samples in each packet received. Must be a multiple of the number of features. Default: 60
    * `--sample-rate`: The number of samples to attempt to collect per second. Default: 50

### Output file options

By default, the server will capture sensor data in a csv based .data file. Alternately, audio data can be captured into a .wav file. With either format, the filename and directory can also be customized.

   * `--data-format <value>`: The type of file to generate. .data for CSV file, or .wav for audio file. Default: .data.
   * `--data-filename <value>`: Name of the data file to be saved. The format will be .data. Default: sensor.data
   * `--output-dir/-o <value>`: Directory where to place the collected data.

### Video configuration options

By default a video file is generated as part of the data collection. The following options are available to customize the video stream. If desired, the video capture functionality can be completely disabled by using the `--video-disabled` argument. Otherwise, the video fps and resolution can be set using the `--video-fps` and `--video-resolution` arguments.

   * `--video-disabled`: Disable video capture. Necessary when capturing audio data 
   * `--video-resolution <value>`: Select web camera video resolution. Default: 540p
   * `--video-fps <value>`: Select FPS of video recording. Default: 20
   * `--video-filename <value>`: Select name of output video files. Default: video.mp4
   * `--opencv-camera-index <value>`: Specify index OpenCV based index of camera stream. Default: 0
