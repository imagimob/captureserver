"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
"""

from acconeer.exptool import utils as acconeer_utils
from acconeer.exptool import configs
from acconeer.exptool.clients import SocketClient, SPIClient, UARTClient

import numpy as np
from captureserver import CaptureServer

from acconeer_sensor_connection import AcconeerSensorConnection


def parse_args():
    parser = acconeer_utils.ExampleArgumentParser()
    parser.prog = "Run Acconeer radar Capture server"

    parser.add_argument(
        "--config",
        dest="config_file",
        type=str,
        required=True,
        help="Path to json config file to configure service with.")

    return parser.parse_args()


def main():
    args = parse_args()
    acconeer_utils.config_logging(args)

    if args.socket_addr:
        client = SocketClient(args.socket_addr)
    elif args.spi:
        client = SPIClient()
    else:
        port = args.serial_port or acconeer_utils.autodetect_serial_port()
        client = UARTClient(port)

    client.squeeze = False

    # Load service configuration and configure the service
    with open(args.config_file, "r") as fs:
        json_string = fs.read()
    service_config = configs.load(json_string)
    print(f"Loaded configuration:\n{'-' * 50}\n{str(service_config)}\n{'-' * 50}")

    sensor_connection = AcconeerSensorConnection(client, service_config)
    capture_server = CaptureServer(sensor_connection)
    capture_server.run()


if __name__ == "__main__":
    main()
