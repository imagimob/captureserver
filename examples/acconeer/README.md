﻿# Acconeer Radar Capture Server Example
This example shows how to integrate the Capture server with the Acconeer XB112/XB122 board which has an Acconeer A1 radar connected.

# Setting up the Acconeer Radar

In order to run the example, you need to setup the Acconeer radar. A guide for this is provided by Acconeer: https://developer.acconeer.com/download/getting-started-guide-xm112-pdf/
  
The steps in the guide should include:

* Installing Acconeer SDK
* Installing Acconeer Python Exploration tools
* Flashing the XB112 or XB122 radar development board - **pay attention to the recommended firmware version below!**
* Connecting the board and running an example

## Firmware Version Info

The current version of the Acconeer Radar Capture server uses **firmware version 2.4** for the XB112/XB122 board. This was the latest available firmware as of 2020-08-26.
  
When bumping the firmware version, then the dependency to acconeer-exploration-tools in ```install_dependencies.sh``` should be updated.

# Running the Acconeer Radar Capture Server

## Installing dependencies

It is recommended to install your dependencies in a virtual python environment. E.g. as such:  

```bash
pip install virtualenv
python -m venv .venv
source .venv/Scripts/activate
```
  
The ```acconeer_radar_capture_server.py``` uses the library in acconeer-python-exploration to communicate with the radar: https://github.com/acconeer/acconeer-python-exploration  
  
This dependency is most easily installed via the provided ```install_dependencies.sh``` script:
```bash
./install_dependencies.sh
```
  
In Windows this script can for example be run using the git-bash shell: https://git-scm.com/
  
If you do not wish to install git-bash, then open the script and run the commands manually in your favorite terminal.

## Starting the Capture server
The Acconeer server can be started according to the example call below:  

```bash
cd examples/acconeer
python acconeer_radar_capture_server.py --uart --config configs/sparse_config.json
```
  
This will start a server with a radar board connected via serial (UART), there is also support for connecting with SPI (```--spi```) and via socket (```--socket [address]```). The above command will use the Sparse service config with parameters defined in the config file, these parameters should be adapted to suit your application. There are more configurations to be found under the ```configs/``` directory, including configs for the services: Envelope and IQ. See the documentation from Acconeer about what the different parameters actually do.

## Starting the local Capture system
Instead of connecting with the Capture app, data can be recorded locally to the PC with video from a web camera:  

```bash
cd examples/acconeer
python acconeer_radar_local_capture.py --uart --config configs/sparse_config.json --output-dir <path to desired output folder>
```
  
Most arguments for the acconeer_radar_capture_server.py can also be applied to this script. You can configure the video fps and resolution using the ```--video-fps``` and ```--video-resolution``` arguments. For even more options and information use the ```--help``` flag.