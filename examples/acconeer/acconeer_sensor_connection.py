"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
"""

from acconeer.exptool.modes import Mode

import numpy as np
from typing import List

from captureserver import SensorConnection


class AcconeerSensorConnection(SensorConnection):
    """
    Class that defines the connection with Acconeer radar sensor.
    """

    def __init__(self, client, service_config):
        self.client = client
        self.service_config = service_config

        # Configure service
        self.service_mode = self.service_config.mode

        print(f'Configuring Acconeer {self.service_mode.value} server.')

        self.service_info = self.client.setup_session(self.service_config)
        self.data_length = self.service_info["data_length"]

        if self.service_mode == Mode.IQ:
            self.data_length = 2 * self.data_length  # real and imaginary parts
            self.data_tag = 'acconeer-iq'
        elif self.service_mode == Mode.ENVELOPE:
            self.data_tag = 'acconeer-envelope'
        elif self.service_mode == Mode.SPARSE:
            self.data_tag = 'acconeer-sparse'
        else:
            raise SystemExit(f"Unsupported service mode: {self.service_mode}")

        # Define json object for Capture App
        self.json_object = [
            {
                "type": "float",
                "count": self.data_length,
                "tag": self.data_tag
            }]

        print("Sensor info: " + ", ".join([f"{k}: {v}" for k, v in self.service_info.items()]))

    def connect(self) -> None:
        self.client.start_session()

    def read_data(self) -> np.ndarray:
        _, float_data = self.client.get_next()
        if self.service_mode == Mode.IQ:
            complex_data = np.array(float_data, dtype=np.complex64)
            float_data = complex_data.view(np.float32)
        elif self.service_mode == Mode.SPARSE:
            # Squeeze and transpose data
            float_data = np.squeeze(float_data)
        return float_data

    def disconnect(self) -> None:
        self.client.disconnect()

    def get_json_packet(self) -> List[dict]:
        return self.json_object
