"""
Copyright (c) 2020 Imagimob AB. Distributed under MIT license.
"""

from pathlib import Path

from acconeer.exptool import utils as acconeer_utils
from acconeer.exptool import configs
from acconeer.exptool.clients import SocketClient, SPIClient, UARTClient

from captureserver import LocalDataCapture
from captureserver.video import OpenCvCameraRecorder
from captureserver.video import RESOLUTIONS

from acconeer_sensor_connection import AcconeerSensorConnection


def parse_args():
    parser = acconeer_utils.ExampleArgumentParser()
    parser.prog = "Run local Acconeer radar data capture"

    parser.add_argument(
        "--config",
        dest="config_file",
        type=str,
        required=True,
        help="Path to json config file to configure service with.")

    parser.add_argument(
        "--output-dir", "-o",
        type=str,
        required=True,
        help="Directory to place collected data.")

    parser.add_argument(
        "--video-resolution",
        default="540p",
        type=str,
        choices=RESOLUTIONS.keys(),
        help="Select web camera video resolution. Default: 540p")

    parser.add_argument(
        "--video-fps",
        default=20.0,
        type=float,
        help="Select FPS of video recording. Recommended: 20 FPS")

    parser.add_argument(
        "--video-filename",
        default="video.mp4",
        type=str,
        help="Select name of output video files. Default: video.mp4")

    parser.add_argument(
        "--opencv-camera-index",
        default=0,
        type=int,
        help="Specify index OpenCV based index of camera stream. Default: 0")

    return parser.parse_args()


def main():
    args = parse_args()
    acconeer_utils.config_logging(args)
    output_dir = Path(args.output_dir)

    # Setup Acconeer client connection
    if args.socket_addr:
        client = SocketClient(args.socket_addr)
    elif args.spi:
        client = SPIClient()
    else:
        port = args.serial_port or acconeer_utils.autodetect_serial_port()
        client = UARTClient(port)
    client.squeeze = False

    # Load service configuration and configure the service
    with open(args.config_file, "r") as fs:
        json_string = fs.read()
    service_config = configs.load(json_string)
    print(f"Loaded configuration:\n{'-' * 50}\n{str(service_config)}\n{'-' * 50}")

    # Setup the video recorder process
    video_resolution = RESOLUTIONS[args.video_resolution]
    video_fps = args.video_fps
    camera_index = args.opencv_camera_index
    video_recorder = OpenCvCameraRecorder(
        visualize=True,
        fps=video_fps,
        video_size=video_resolution,
        opencv_camera_index=camera_index)

    # Run the local capture system
    sensor_connection = AcconeerSensorConnection(client, service_config)
    data_filename = sensor_connection.get_json_packet()[0]["tag"] + ".data"
    video_filename = args.video_filename
    local_data_capture = LocalDataCapture(
        output_dir=output_dir,
        sensor_connection=sensor_connection,
        video_recorder=video_recorder,
        data_filename=data_filename,
        video_filename=video_filename)
    local_data_capture.run()


if __name__ == "__main__":
    main()
