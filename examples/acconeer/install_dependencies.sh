# TODO: If possible this would be best to add as a dependency
#       to the requirements.txt file...

# Bump this sha-id from the acconeer-python-exploration to
# update this dependency
CURRENT_VERSION=cdfd9e0ed9f6d9ec6f987a763f26af659ee80718

git clone https://github.com/acconeer/acconeer-python-exploration.git
cd acconeer-python-exploration
git checkout $CURRENT_VERSION
pip install -r requirements_client_only.txt
python setup.py install
cd ..
rm -rf acconeer-python-exploration
